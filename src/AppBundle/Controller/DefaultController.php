<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AbstractArticle;
use AppBundle\Entity\AnonymousArticle;
use AppBundle\Entity\Article;
use AppBundle\Entity\Tag;
use AppBundle\Exception\UnsupportedArticleTypeException;
use AppBundle\Form\AnonymousArticleForm;
use AppBundle\Form\ArticleForm;
use AppBundle\Repository\ArticleRepository;
use AppBundle\Service\Slug\SlugGeneratorInterface;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $articlesRepository = $this->getDoctrine()->getRepository(AbstractArticle::class);
        $articles = $articlesRepository->findAll();

        return $this->render('default/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/articles", name="articles")
     */
    public function articlesIndexAction()
    {
        $articlesRepository = $this->getDoctrine()->getRepository(Article::class);
        $articles = $articlesRepository->findAll();

        return $this->render('authors/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/tags", name="tags")
     */
    public function tagsAction()
    {
        $tags = $this->getDoctrine()->getRepository(Tag::class)->findAll();

        return $this->render('tags/index.html.twig', [
            'tags' => $tags
        ]);
    }


    /**
     * @Route("/anonymous-article-add", name="create-anonymous")
     */
    public function anonymousArticleAddAction(Request $request)
    {
        $article = new AnonymousArticle();

        $form = $this->createForm(AnonymousArticleForm::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('show', ['slug' => $article->getSlug()]);
        }

        return $this->render('common/form.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article-add", name="create")
     */
    public function authorArticleAddAction(Request $request)
    {
        $article = new Article();

        $form = $this->createForm(ArticleForm::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('show', ['slug' => $article->getSlug()]);
        }

        return $this->render('common/form.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{slug}", name="show")
     */
    public function showAction(string $slug)
    {
        /**@var $articlesRepository ArticleRepository */
        $articlesRepository = $this->getDoctrine()->getRepository(AbstractArticle::class);
        try {
            $article = $articlesRepository->findOneBySlug($slug);
            switch (true) {
                case $article instanceof Article:
                    $viewName = 'authors/details.html.twig';
                    break;

                case $article instanceof AnonymousArticle:
                    $viewName = 'anonymous/details.html.twig';
                    break;

                default:
                    throw new UnsupportedArticleTypeException();
            }
            return $this->render($viewName, [
                'article' => $article,
            ]);
        } catch (EntityNotFoundException $e) {
            throw $this->createNotFoundException('Article not found. ');
        }
    }

    /**
     * @Route("/articles-by-tag/{tagId}", name="articles-by-tag")
     */
    public function articlesByTag($tagId)
    {
        $tag = $this->getDoctrine()->getRepository(Tag::class)->find($tagId);
        $articles = $tag->getArticles();

        return $this->render('tags/articles-by-tag.html.twig', [
            'articles' => $articles,
            'tag' => $tag,
        ]);
    }

    /**
     * @Route("/edit-article/{articleId}", name="edit-article")
     */
    public function editArticle($articleId, Request $request)
    {
        $article = $this->getDoctrine()->getRepository(AbstractArticle::class)->find($articleId);
        switch (true) {
            case $article instanceof Article:
                $form = $this->createForm(ArticleForm::class, $article);
                break;

            case $article instanceof AnonymousArticle:
                $form = $this->createForm(AnonymousArticleForm::class, $article);
                break;

            default:
                throw new UnsupportedArticleTypeException();

        }
        $form->add('save', SubmitType::class,
            [
                'label' => 'Save',
                'attr' => ['class' => 'form-control']
            ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
        }

        return $this->render('common/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

}

<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class ArticleForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('text', TextareaType::class, ['attr' => ['class' => 'form-control']])
            ->add('publishDate', DateType::class, ['attr' => ['class' => 'form-control']])
            ->add('authorName', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('externalLink', UrlType::class, ['attr' => ['class' => 'form-control']])
            ->add('save', SubmitType::class,
                [
                    'label' => 'Create',
                    'attr' => ['class' => 'form-control']
                ])->setAttribute('class', 'form-group');
    }
}
<?php

namespace AppBundle\Form;

use AppBundle\Entity\AnonymousArticle;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnonymousArticleForm extends AbstractType
{

    private $entityManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->entityManager = $objectManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('text', TextareaType::class, ['attr' => ['class' => 'form-control']])
            ->add('publishDate', DateType::class, ['attr' => ['class' => 'form-control']])
            ->add('tags', TextType::class,
                ['required' => false, 'attr' => ['class' => 'form-control']])
            ->add('save', SubmitType::class,
                [
                    'label' => 'Create',
                    'attr' => ['class' => 'form-control']
                ]);

        $builder->get('tags')->addModelTransformer(new TagCollectionDataTransformer($this->entityManager));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AnonymousArticle::class,
        ));
    }
}
<?php

namespace AppBundle\Form;

use AppBundle\Entity\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class TagCollectionDataTransformer implements DataTransformerInterface
{

    private $entityManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->entityManager = $objectManager;
    }

    /**
     * @inheritdoc
     * @param ArrayCollection $value The value in the original representation
     * @return mixed The value in the transformed representation
     * @throws TransformationFailedException When the transformation fails.
     */
    public function transform($value)
    {
        return \implode(', ', \array_map(function (Tag $tag) {
            return $tag->getName();
        }, $value->toArray()));
    }

    /**
     * @inheritdoc
     * @param mixed $value The value in the transformed representation
     * @return mixed The value in the original representation
     * @throws TransformationFailedException When the transformation fails.
     */
    public function reverseTransform($value)
    {
        $tagNames = $this->parseInputData((string)$value);
        return $this->entityManager->getRepository(Tag::class)->buildTagCollection($tagNames);
    }

    private function parseInputData(string $value): array
    {
        $tagNames = \explode(',', $value);
        $tagNames = \array_map(function (string $tagName): string {
            return \trim($tagName);
        }, $tagNames);

        return $tagNames;
    }
}
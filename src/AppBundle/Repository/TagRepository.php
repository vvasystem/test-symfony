<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class TagRepository extends EntityRepository
{


    /**
     * Cet or create tags from in collection
     * @param array|string[] $tagNames
     * @return ArrayCollection
     */
    public function buildTagCollection(array $tagNames): ArrayCollection
    {
        $existingTags = new ArrayCollection($this->findBy([
            'name' => $tagNames
        ]));
        $nonExistingTagNames = \array_filter($tagNames, function (string $tagName) use ($existingTags) {
            return !$existingTags->exists(function ($key, Tag $tag) use ($tagName) {
                return $tag->getName() === $tagName;
            });
        });
        // remove empty values
        $nonExistingTagNames = \array_filter($nonExistingTagNames);
        foreach ($nonExistingTagNames as $newTagName) {
            $newTag = new Tag();
            $newTag->setName($newTagName);
            $this->getEntityManager()->persist($newTag);
            $existingTags->add($newTag);
        }

        $this->getEntityManager()->flush();

        return $existingTags;
    }
}
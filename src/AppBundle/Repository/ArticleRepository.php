<?php

namespace AppBundle\Repository;

use AppBundle\Entity\AbstractArticle;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;

class ArticleRepository extends EntityRepository
{

    public function findOneBySlug(string $slug): AbstractArticle
    {
        /**@var AbstractArticle $article */
        if ($article = $this->findOneBy(['slug' => $slug])) {
            return $article;
        }
        throw new EntityNotFoundException();
    }

}
<?php

namespace AppBundle\Service\Slug;

use AppBundle\Service\Transliterator\TransliteratorInterface;

class TransliterationWithTimestampSlugGenerator implements SlugGeneratorInterface
{

    /**
     * @var TransliteratorInterface
     */
    private $transliterator;

    public function __construct(TransliteratorInterface $transliterator)
    {
        $this->transliterator = $transliterator;
    }

    public function generateSlug(string $string): string
    {
        $string = $this->transliterator->tranliterate($string);
        // replace non letter or non digits by -
        $string =\ preg_replace('#[^\\pL\\d]+#u', '-', $string);
        // Trim trailing -
        $string = \trim($string, '-');
        $clean = \preg_replace('~[^-\\w]+~', '', $string);
        $clean = \strtolower($clean);
        $clean = \preg_replace('#[\\/_|+ -]+#', '-', $clean);
        return \trim($clean, '-') . '-' . \time();
    }

}
<?php

namespace AppBundle\Service\Slug;

interface SlugGeneratorInterface
{
    public function generateSlug(string $string): string;
}
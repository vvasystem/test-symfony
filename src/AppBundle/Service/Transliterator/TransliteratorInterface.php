<?php

namespace AppBundle\Service\Transliterator;

interface TransliteratorInterface
{

    public function tranliterate(string $string): string;

}
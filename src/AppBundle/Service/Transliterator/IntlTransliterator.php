<?php

namespace AppBundle\Service\Transliterator;

class IntlTransliterator implements TransliteratorInterface
{

    private $intlTransliterator;

    public function __construct(string $id, int $direction = null)
    {
        $this->intlTransliterator = \Transliterator::create($id, $direction);
    }

    public function tranliterate(string $string): string
    {
        return $this->intlTransliterator->transliterate($string);
    }
}
<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class AnonymousArticle extends AbstractArticle
{
    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="articles")
     */
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        parent::__construct();
    }

    public function setTags(ArrayCollection $tags)
    {
        $this->tags = $tags;
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function getTagsString(): string
    {
        return join(', ', array_map(function (Tag $tag) {
            return $tag->getName();
        }, $this->tags->toArray()));
    }
}
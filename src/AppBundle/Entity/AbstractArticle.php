<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"anonymous" = "AnonymousArticle", "author" = "Article"})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArticleRepository")
 */
abstract class AbstractArticle
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="date")
     */
    private $publishDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return (string)$this->name;
    }

    public function setText(string $text)
    {
        $this->text = $text;
    }


    public function getText(): string
    {
        return (string)$this->text;
    }

    public function getPublishDate()
    {
        return $this->publishDate;
    }

    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }


    public function getSlug(): string
    {
        return (string)$this->slug;
    }

    public function setPublishDate(\DateTime $publishDate)
    {
        $this->publishDate = $publishDate;
    }

    public function getId()
    {
        return $this->id;
    }

}
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Article extends AbstractArticle
{

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $externalLink;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $authorName;

    public function getExternalLink()
    {
        return $this->externalLink;
    }

    public function setExternalLink(string $externalLink)
    {
        $this->externalLink = $externalLink;
    }

    public function getAuthorName()
    {
        return $this->authorName;
    }

    public function setAuthorName(string $authorName)
    {
        $this->authorName = $authorName;
    }
}
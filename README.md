# ТЗ 

Создать сайт простого онлайн-журнала на Symfony 2. Без дизайна. 

На сайте должно быть страницы:
- Список всех статей;
- Список всех авторских статей;
- Статьи по конкретному тегу;
- Детальная страница статьи (в ссылке должно быть ЧПУ);
- Страница создания статьи;
- Страница редактирования статьи. 

У статьи должны быть поля:
- Название;
- Текст;
- Дата публикации.

Скрытые поля:
- ЧПУ (должно генерироваться из названия статьи);
- Время создания;
- Время изменения.

У тегов:
- Название;
- Время создания. 

Должна быть возможность создавать два типа статей:
- Обычная статья (к такой статье можно привязывать несколько тегов, не больше 3-х);
- Авторская (у этой статьи дополнительные поля: автор и ссылка на сайт, но нет тэгов). 

Тэги при редактирование статьи пишутся в одном поле через запятую. 

Дополнительные поля у каждого варианта должны храниться не в общей таблице.

Контроллеры должны быть максимально тонкими. 

Можно использовать любые библиотеки подходящие для симфонии

## Requirements:
- Installed docker-compose 
- Current user in docker group (or sudo), `usermod -a -G docker $USER`
- Installed composer (or use docker image)
- Free `8000` port (can be changed in docker-compose.yml)   

## Install and Run
- `git clone https://git@bitbucket.org/vvasystem/test-symfony.git`
- `cd test-symfony/`
- `./composer.sh install` (or `composer install` if don't use docker image)
- `docker-compose up -d`
- `./app/console.sh doctrine:schema:update --force` - if you have got the error `SQLSTATE[HY000] [2002] Connection refused`, please try again later (it's normal because mysql not yet started)
- [http://127.0.0.1:8000/app_dev.php]